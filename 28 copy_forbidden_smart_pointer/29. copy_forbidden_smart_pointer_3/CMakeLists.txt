cmake_minimum_required(VERSION 3.3)
project(29__copy_forbidden_smart_pointer_3)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp)
add_executable(29__copy_forbidden_smart_pointer_3 ${SOURCE_FILES})