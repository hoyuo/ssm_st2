#include <iostream>
#include <list>
#include <vector>

using namespace std;

template <typename T, template <typename, typename> class C>
class Stack
{
    C<T, allocator<T>> c;
public :
    void push(const T& a)
    {
        c.push_back(a);
    }
};

int main()
{
    Stack<int, list> s;
    // list는 2개의 템플릿인자가 필요하다.
    return 0;
}