// 템플릿인자2

// 3. template 인자로 template을 보낼 수 있다
#include <iostream>

using namespace std;

template <typename T>
class list
{
};

// Android - cor/include/utils/List.h
template <typename T, template <typename> class C>
class stack
{
    //C    c; //error. C는 template 이다.
    C<T> c; //OK!
};

int main()
{
    //    list      st1;  // error : T를 알 수가 없다. list는 template이다.
    list<int> st2;  //ok.       list<int>는 type이다.

    stack<int, list> s;
    return 0;
}