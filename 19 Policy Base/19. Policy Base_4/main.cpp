// 단위전략4
#include <iostream>
#include <string>

using namespace std;

//stirng 클래스의 모양은 아래와 유사합니다.
/*
 * template <typename

class basic_string {
    T* buff;
    Alloc alloc;
};
*/

// basic_string 이 사용할 비교 단위 전략
// 단위 전략 만들때 일부 함수의 정책만 변경하려면 기본 단위전략의 자식으로 만들어도 됩니다.
struct my_traits : public char_traits<char> {
    static bool eq(char a, char b) { return toupper(a) == toupper(b); }
    static bool lt(char a, char b) { return toupper(a) < toupper(b); }
    static bool gt(char a, char b) { return toupper(a) > toupper(b); }
    static bool compare(const char* a, const char* b, int sz) { return memcmp(a, b, sz); }
};

typedef basic_string<char, my_traits, allocator<char>> my_string;

int main() {
    my_string s1 = "ABCD";
    my_string s2 = "abcd";

    if (s1 == s2) {
        cout << "Same" << endl;
    } else {
        cout << "not same" << endl;
    }

    return 0;
}