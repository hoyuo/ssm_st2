# auto
auto : 우변의 타입으로 좌변 변수 타입을 컴파일 시간에 결정한다. 단, 초기값이 있어야 한다.  

```decltype(parameter) variable;``` : parameter의 타입으로 variable를 만들어 달라. 초기값이 없어도 가능하다.

```c++
#include <iostream>
using namespace std;

int x = 10;
int &foo() { return x; }

int main() {
    int n1 = 10;
    // int n2 = n1;

    auto n2 = n1;   // 우변의 타입으로 좌변 변수 타입을 컴파일 시간에 결정해 달라
    //초기값이 있어야 한다.

    decltype(n1) n_2;   // n1의 타입으로 n2를 만들어 달라.
    //초기값이 없어도 가능하다.

    auto n3 = foo();    // int ? int& ? 중 뭘까요?
    // int

    decltype(foo()) n4 = foo(); // int&

    return 0;
}
```