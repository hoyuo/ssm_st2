# auto
auto : 우변의 타입으로 좌변 변수 타입을 컴파일 시간에 결정한다. 단, 초기값이 있어야 한다.  

```decltype(parameter) variable;``` : parameter의 타입으로 variable를 만들어 달라. 초기값이 없어도 가능하다.
