#include <iostream>

using namespace std;

int x = 10;

int& foo() {
    return x;
}

int main() {
    auto n = foo();
//    규칙 1. 우변의 타입중
//    const, reference, volatile을 버려라
//    n의 타입은 ? template 과 동일
//    auto  : T
//    n     : param
//    expr  : = foo()
    n = 20;
    cout << x << endl;

    auto& r = foo();
//    규칙 2. 우변의 속성중 레퍼런스 무시
//    n의 타입은 ? template 과 동일
//    auto  : int
//    r     : int&
    r = 20;
    cout << x << endl;

    return 0;
}