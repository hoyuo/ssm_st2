#include <iostream>

using namespace std;

// 규칙 3. Universal reference 인 경우
// int&& : rValue reference - rValue만 담을수 있다.
// T&&   : universal reference ( forward reference 라고도 합니다.)

// 인자로 전달되는 표현이 lValue 이면 T&&는 lValue reference 가 되고
// 인자로 전달되는 표현이 rValue 이면 T&&는 rValue reference 가 된다.
template<typename T>
void foo(T&& a) { // int&
}

int main() {
    int n = 10;
    foo(n);
    foo(10);

    int& r = n;
    foo(r);

    return 0;
}