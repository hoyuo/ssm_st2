// 임시객체
// 함수 인자와 임시객체
// 값타입으로 리턴하는 함수는 임시객체를 만들게 된다.
#include <iostream>

using namespace std;

class Point {
    int x, y;
public :
    Point() { cout <<"생성자" << endl; }
    ~Point() { cout << "소멸자" << endl; }

    Point(const Point&) { cout << "복사생성자" << endl; }
};

//Point foo() {
//    Point p2;
//    cout << "foo" << endl;
//    return p2;
//}

Point foo() {
//    Point p2;
    cout << "foo" << endl;
//    return p2;
    return Point(); //임시객체로 만들면서 리턴 !!
//    Return Value Optimization( RVO )라고 불리는 기술
//    C++ IDioms

//    요즘 C++ 컴파일러 : NRVO를 지원한다.
//    Named RVO
}

int main() {

    Point p1;
    cout << "AAA" << endl;

    p1 = foo();
    cout << "BBB" << endl;

    return 0;
}

// 실행하지 말고.. 화면 출력 결과 예측해 보세요.
/*
 * 1. 생성자 p1
 * 2. AAA
 * 3. 생정자 p2
 * 4. foo
 * 5. 복사생성자 - 리턴용 임시객체
 * 6. 소멸자 - p2
 * 7. 소멸자 - 임시객체용
 * 8. BBB
 * 9. 소멸자 - p1
 */