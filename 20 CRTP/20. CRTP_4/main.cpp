#include <iostream>

using namespace std;

class Count {
public:
    static int count;
    static void print_count() { cout << count << endl; }

    Count() { ++count; }
    ~Count() { --count; }
};

int Count::count = 0;

int main() {
    Count c1, c2;

    c1.print_count();
    return 0;
}