/* CRTP : Curiously Recurring Template Patter
 *        부모가 템플릿인데 자식 만들때 자신의 이름을 부모에게 전달해주는 기술
 *        미래에 만들어질 자식의 이름을 사용할 수 있다.
 */

// CRTP를 사용해서 비 가상함수를 가상함수처럼 동작하게 하는 예제
// MS의 라이브러리 중 ATL, WTL 이라는 라이브러리가 이 기술을 사용!

#include <iostream>

using namespace std;

template<typename T>
class Window {
public :
    void msgLoop() { // msgLoop( Window* this )
//        onClick(); // this->onClick();
        (static_cast<T*>(this))->onClick();
    } // 1 ? 2 -> 1번을 호출한다.

    void onClick() { cout << "Window onClick" << endl; } // 1
};

class MyWindow : public Window<MyWindow> {
public:
    void onClick() { cout << "MyWindow onClick" << endl; } // 2
};

int main() {
    MyWindow w;
    w.msgLoop();
    return 0;
}