#include <iostream>

using namespace std;

//싱글톤 : 오직 하나의 객체만 생성되게 하는 디자인 패턴

class Cursor {
private :
    Cursor() { }
    static Cursor* pInstance;
public :
// 오직 하나의 객체만 만들어서 리턴하는 정적 함수
    static Cursor& getInstance() {
        if (pInstance == 0) {
            pInstance = new Cursor;
            return *pInstance;
        }
    }
};

int main() {
    Cursor& c1 = Cursor::getInstance();
    Cursor& c2 = Cursor::getInstance();

    cout << &c1 << " ," << &c2 << endl;
    return 0;
}