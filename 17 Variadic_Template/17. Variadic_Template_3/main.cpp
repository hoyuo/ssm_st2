#include <iostream>

using namespace std;

template<typename T>
int xtuple_size(T& a) { return T::N; }

// 결국 아래의 primary 버전은 사용되지 않고
// 부분 전문화 버전만 사용하게 됩니다.
// 이경우 구조체 몸체({ }) 없이 선언만 있어도 됩니다.
// 단, 선언자체는 꼭 있어야 부분전문화 버전을 만들수 있습니다.

template <int N, typename T, typename  ... Types>
struct xtuple_type;

// 0번째 타입을 요구할때 부분 전문화
template<typename T, typename ... Types>
struct xtuple_type<0, xtuple_type < T, Types...>>
{
typedef T type;
}

// 0이 아닌 경우
template<int N, typename T, typename ... Types>
struct xtuple_type<N, xtuple< T, Types...>> : xtuple_type<N-1, xtuple<Types...>>
{
typedef T type;
}


// xtuple_type을 확인 하기 위한 함수
template<int N, typename T> void print_type(const T& a) {
    cout << typeid(xtuple_type<N, T>::type).name() << endl;
};

int main() {
    xtuple<int, char, double, short> t4(1, 'c', 3.3, 4);
    // tuple 타입 알아내기
    print_type<2>(t4);

    // 숙제
    cout << xget<1>(t4) << endl;
}