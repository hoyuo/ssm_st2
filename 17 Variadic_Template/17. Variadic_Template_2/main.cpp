// 가변인자 템플릿

#include <iostream>

using namespace std;

// Types : 여러개의 타입을 가지고 있는 타입의 집합
// args  : 여러개의 값을 가지고 있는 인자 집합 - "Parameter Pack"

int n = 0;

// 결국 아래 템플릿은 foo()함수가 4개 생성됩니다.
// 인자 값을 꺼내려면 1번째 인자는 이름이 있는 변수로 받아야 한다.
template <typename T, typename ... Types>
void foo(T value, Types ... args) {
//    static int n = 0;
    ++n;
    cout << n << " ";

    cout << typeid(T).name() << " : " << value << endl;

    foo(args...);
    /* args : 3.3, 'a', "hello"
     * args : 'a', "hello"
     * args : "hello"
     * args :
     */

}

// 인자가 없는 경우
void foo() {
}

int main() {
    foo(1, 3.3, 'a', "hello"); // value : 1 args : 3.3, 'a', "hello"
    return 0;
}