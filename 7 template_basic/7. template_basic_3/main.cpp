#include <iostream>

using namespace std;

template <typename T>
T square(T a)
{
    return a * a;
}

// square : 함수를 만드는 틀(template)
// square<int> : 함수!
int main()
{
    printf("%p\n", &square); //될까요? 잘 생각해보세요 -> error :
    printf("%p\n", &square<int>); //될까요? 잘 생각해보세요 -> ok
    return 0;
}
