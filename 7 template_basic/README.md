# Template Basic

## 1. 함수 오버로딩
함수 사용자 : 하나의 함수처럼 보인다
함수 제작자 : 2개의 함수를 만들어야 한다.


```c++
#include <iostream>

using namespace std;

int square ( int a ) {
    return a * a;
}

double square ( double a ) {
    return a * a;
}


int main ( ) {
    square (3);
    square (3.3);
    return 0;
}
```
다음과 같이 진행을 하지 않고 코드 생성을 통해서 쉽게 진행을 한다.

```c++
#define MAKE_SQUARE( T ) T square(T a) {return a* a;}

MAKE_SQUARE(int)
MAKE_SQUARE(double)
```
전처리기가 값을 적는다.