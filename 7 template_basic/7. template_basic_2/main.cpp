#include <iostream>

using namespace std;

template <typename T>
T square(T a)
{
    return a * a;
}

int main()
{
    short s = 3;

    square(3); // int 버전
    square(s); // short 버전
    return 0;
}

// asm을 통해서 코드를 확인해보자