//전달 받은 T가 추상함수인지, 포인터 인지 여러가지 판별하는것을 traits 라 한다.

#include <iostream>

using namespace std;

//template <typename T>
//void printv(T a)
//{
//    cout << a << endl;
//}

template <typename T>
void printv(T a)
{
    if (T is Pointer)
        cout << a << ", " << *a << endl;
    else
        cout << a << endl;
}

int main()
{
    int    n = 3;
    double d = 3.3;

    printv(n);
    printv(d);

    printv(&d);

    return 0;
}