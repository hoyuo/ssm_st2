#include <iostream>
#include <type_traits>

using namespace std;

// C++ 표준위원회는 int2type을 발전시켜서 아래 템플릿을 제공합니다.

/*
template <typename T, T N>
struct integral_constant
{
//    enum
//    {
//        value = N
//    };

    static const T value = N;
    //    static const는 클래스안에서 바로 초기화 가능합니다.
    //    2000년 초반에 추가된 문법
    //    그래서 요즘은 enum 대신 사용합니다
};

//integral_constant<int, 0>   n0;
//integral_constant<short, 0> s0;

// true / false           : 참 거짓을 나타내는 값 - 같은타입
// true_type / false_type : 참 거짓을 나타내는 타입 - 다른 타입

typedef integral_constant<bool, true>  true_type;
typedef integral_constant<bool, false> false_type;

// 이제 is_pointer 등을 만들때 아래처럼 합니다.
// 상속의 개념이 추가되고, value는 부모로 부터 물려 받습니다.
template <typename T>
struct is_pointer
        : false_type
{
};

template <typename T>
struct is_pointer<T *>
        : true_type
{
};
*/
// 윗 부분은 이미 다 C++에서 구현이 되어있다. #include <type_traits>를 이용해서 진행을 하면 된다.

template <typename T>
void printv_imp(T a, true_type)
{
    cout << a << ", " << *a << endl;
}

template <typename T>
void printv_imp(T a, false_type)
{
    cout << a << endl;
}

template <typename T>
void printv(T a)
{
    printv_imp(a, is_pointer<T>());
}

int main()
{
    int n = 3;

    printv(n);
    printv(&n);

    return 0;
}