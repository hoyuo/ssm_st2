#include <iostream>

using namespace std;

int a;
int * p;
int x[10]; // int [10]가 x의 타입이다.
//T[N]


// traits 만드는 방법 다시 정리

// primary template : false_type을 기본 클래스로
// 부분 전문화 버전 : true_type을 기본 클래스로
template <typename T>
struct IsArray
        : false_type
{
    static const int size = -1;
};

template <typename T, int N>
struct IsArray<T[N]>
        : true_type
{
    static const int size = N;
};

template <typename T>
void foo(const T& a)
{
    if ( IsArray<T>::value )
    {
        cout << "Array" << IsArray<T>::size << endl;
    }
    else
    {
        cout << "NO Array" << IsArray<T>::size << endl;
    }
}

int main()
{
    int x[10];
    foo(x);
    return 0;
}