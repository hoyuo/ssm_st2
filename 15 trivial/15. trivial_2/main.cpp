#include <iostream>
#include <type_traits>

using namespace std;

// 모든 타입의 배열을 복사 하는 strcpy()의 일반화 버전을 만들어 봅시다.

//template <typename T>
//void copy_type(T * d, T * s, int sz)
//{
//    memcpy(d, s, sizeof(T) * sz);
//}
// 위와 같이 코드를 작성했을때 문제가 있다.
// 객체가 있을 경우 그 객체의 얉은 복사만 진행되고 있다.

template <typename T>
void copy_type(T * d, T * s, int sz)
{
    // vc : is_trivially_copyable
    if ( !is_trivially_copy_constructible<T>::value )
    {
        cout << "복사 생성자가 하는 일이 있을 때" << endl;
        while (sz--)
        {
            // new(d) T; // 디폴트 생성자 호출
            new(d) T(*s); // d 객체에 대해서 복사 생성자(*s 인자로) 호출
            ++d;
            ++s;
        }
    }
    else
    {
        cout << "복사 생성자가 하는 일이 없을때" << endl;
        memcpy(d, s, sizeof(T) * sz);
    }
}

int main()
{
    char s1[10] = "hello";
    char s2[10];

    //    strcpy(s2, s1);

    copy_type(s2, s1, 10);

    return 0;
}