#include <iostream>

using namespace std;

// trivial traits를 만드는 일반적인 기술은 어렵습니다.
// Android Framework 에서는

template <typename T>
struct has_trivial_ctor
{
    enum
    {
        value = false
    };
    // 모든 생성자는 trivial 하지 않다 라는 전제 조건
};

template < > struct has_trivial_ctor<int> { enum { value = true }; };
template < > struct has_trivial_ctor<short> { enum { value = true }; };
template < > struct has_trivial_ctor<char> { enum { value = true }; };
template < > struct has_trivial_ctor<double> { enum { value = true }; };
template < > struct has_trivial_ctor<float> { enum { value = true }; };

struct Point
{
    int x, y;
};
template < > struct has_trivial_ctor<Point> { enum { value = true }; };

void foo()
{
    if ( has_trivial_ctor<int>::value )
    {

    }

}

int main()
{
    cout << "Hello, World!" << endl;
    return 0;
}