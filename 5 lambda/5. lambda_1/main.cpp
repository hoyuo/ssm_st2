#include <iostream>

using namespace std;

int Add1(int a, int b) { return a + b; }

inline int Add2(int a, int b) { return a + b; }

int main() {
    int n1 = Add1(1, 2); // 호출
    int n2 = Add2(1, 2); // 기계어 코드 치환 - 속도가 빨라진다.

    int(*f)(int, int);

    f = &Add2;

    // ------
    // int n = 0;
    // cin >> n;
    // if (n == 1) f = &Add1;
    // -------

    int n3 = f(1, 2);
    // inline 치환 안됨
    // f(1,2)는 Run-time 문법이기 때문이다. 

    // vc
    // cl main.cpp /Ob1 /FAs 대문자 "O"
    // Ob1 : 인라인 치환을 적용해 달라 FAs : 어셈 소스를 만들어 달라

    // gcc
    // g++ main.cpp -s

}