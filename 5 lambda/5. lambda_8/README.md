## 8. 람다를 인자로 받는 방법
람다는 3가지 형태 변수에 담을 수 있습니다.  
```auto f1 = [](int a, int b) { return a + b; };``` // inline 치환이 가능하다. 유일하게    
```int(*f2)(int, int) = [](int a, int b) { return a + b; };```  
```function<int(int, int)> f3 = [](int a, int b) { return a + b; };```  