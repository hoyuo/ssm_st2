#include <iostream>
#include <functional>

using namespace std;

// 람다를 인자로 받는 방법 - 아래 2개 가능하지만 인라인 치환 안됨
// void foo(int(*f2)(int, int)) { }
// void foo(function<int(int, int)> f3) { }

// void foo(auto f1) { }//auto는 절대 함수 인자가 될수 없다. 타입을 모르기 때문이다.

// 인라인 치환 가능하게 람다를 받는 유일한 방법은 - template
template<typename T>
void foo(T f) {
    f(1, 2);
}

int main() {
    // 람다는 3가지 형태 변수에 담을 수 있습니다.
    auto f1 = [](int a, int b) { return a + b; }; // inline 치환이 가능하다. 유일하게

    int(*f2)(int, int) = [](int a, int b) { return a + b; };

    function<int(int, int)> f3 = [](int a, int b) { return a + b; };

    cout << f1(1, 2) << endl;
    cout << f2(1, 2) << endl;
    cout << f3(1, 2) << endl;


    foo([](int a, int b) { return a + b; });
    return 0;
}

