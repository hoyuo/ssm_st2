#include <iostream>

using namespace std;

// 멤버 변수 캡쳐

class Test {
    int base;
public:
    Test(int n) : base(n) { }

    void foo() {
        // 멤버 변수인 base를 캡쳐하려면?
        // auto f = [base]() { cout << base << endl; };  // error

        // 참조 캡쳐는 아니지만 포인터를 값으로 했으므로 변경 가능.
        // 숙제 1. 아래 코드를 사용했을때 컴파일러가 만들어 내는 클래스를 예측해 보세요.
        auto f = [this]() {
            base = 200;
            cout << base << endl;
        };  // ok
        f();
    }
};

int main() {
    Test t(20);
    t.foo();
    return 0;
}