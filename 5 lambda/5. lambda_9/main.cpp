#include <iostream>

// 람다와 리턴타입
using namespace std;

int main() {
    auto f1 = [](int a, int b) { return a + b; };

    auto f2 = [](int a, int b) { return a > b ? a : b; };

    // 리턴 타입의 인자 () 뒤에 적는 문법 - trailing return
    auto f3 = [](int a, int b) -> double { return a > b ? a : 3.0; };


    return 0;
}