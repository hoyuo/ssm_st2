cmake_minimum_required(VERSION 3.2)
project(5__lambda_4)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp)
add_executable(5__lambda_4 ${SOURCE_FILES})