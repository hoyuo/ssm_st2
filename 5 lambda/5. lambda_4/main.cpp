#include <iostream>

using namespace std;

struct less {
    inline bool operator()(int a, int b) { return a < b; }
};

struct greater {
    inline bool operator()(int a, int b) { return a > b; }
};

// 아래 함수는 cmp()가 인라인 치환되지만 greater로 교체할 수 없다.
// void Sort(int *x, int n, less cmp) {

// 정책교체가 가능하고 정책이 인라인 치환되는 함수
// "템플릿 + 함수객체"를 사용한 기술.!
template<typename T>
void Sort(int *x, int n, T cmp) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            if (cmp(x[i], x[j])) { // 인라인 치환 가능
                swap(x[i], x[j]);
            }
        }
    }
}

int main() {
    int x[10] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10};

    less f1;
    greater f2;

    Sort(x, 10, f1);
    Sort(x, 10, f2);
}