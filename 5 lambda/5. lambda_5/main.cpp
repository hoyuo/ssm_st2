#include <iostream>
#include <algorithm> // 정책변경이 가능한 sort()가 여기 있다.

using namespace std;

// Sort() 사용자는 비교 함수를 전달해야 한다.
inline bool cmp1(int a, int b) { return a < b; }

inline bool cmp2(int a, int b) { return a > b; }

int main() {
    int x[10] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10};
    // sort의 모든 인자는 템플릿 입니다.

    // 1. 비교정책으로 일반 함수를 사용하는 경우
    // 장점 : 정책을 여러번 교체 해도 sort() 기계어는 한개이다. 메모리 사용량 감소
    // 단점 : 정책이 인라인 치환될수 없기 때문에 성능저하가 있다.

    sort(x, x + 10, cmp1);  // sort(int*, int*, bool(*)(int, int)) 함수 생성
    sort(x, x + 10, cmp2);  // sort(int*, int*, bool(*)(int, int)) 함수 생성

    // 2. 비교 정책으로 함수객체를 사용하는 경우
    // 장점 : 정책이 인라인 치환 되므로 속도가 빠르다
    // 단점 : 정책을 교체한 횟수 만큼의 sort() 함수 생성, 코드 메모리 증가

    less<int> f1;
    greater<int> f2;
    sort(x, x + 10, f1);  // sort(int*, int*, less) 함수 생성
    sort(x, x + 10, f2);  // sort(int*, int*, greater) 함수 생성

    // 3. c++11/14 람다 표현식(lambda experssion)
    // 함수 인자로 함수의 구현(코드)를 전달하는 기술
    // [] : lambda introducer
    
    sort(x, x + 10, [](int a, int b) { return a < b; });
    return 0;
}