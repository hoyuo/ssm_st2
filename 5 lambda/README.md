# lamda

## 1. 인라인 함수와 함수 포인터 관계
* 핵심 1. 인라인 치환은 컴파일 시간 문법이다.
* 핵심 2. 인라인 함수라도 함수 포인터에 담아서 사용하면 인라인 치환 될 수 없다.
* vc
>cl main.cpp /Ob1 /FAs 대문자 "O"  
>Ob1 : 인라인 치환을 적용해 달라 FAs : 어셈 소스를 만들어 달라

* gcc
> g++ main.cpp -s

```c++
#include <iostream>
using namespace std;

int Add1(int a, int b) { return a + b; }

inline int Add2(int a, int b) { return a + b; }

int main() {
    int n1 = Add1(1, 2); // 호출
    int n2 = Add2(1, 2); // 기계어 코드 치환 - 속도가 빨라진다.
    int(*f)(int, int);
    f = &Add2;
    //------
    //int n = 0;
    //cin >> n;
    //if (n == 1) f = &Add1;
    //-------
    int n3 = f(1, 2); // inline 치환 안됨.
}
```

## 2. 정책 변경이 가능한 함수 만들기
S/W 설계의 기본 원칙 : 변하지 않은 전체 알고리즘 에서 변해야 하는 부분을 찾아서 분리 한다.  
일반함수는 변하는 부분을 함수 인자화 한다. (함수 포인터)  
C 표준 함수인 qsort()가 아래 모양입니다. 핵심은 "알고리즘과 정책의 분리" 입니다.  
장점 : 함수가 사용하는 정책을 사용자가 변경할 수 있다.  
단점 : 결국 callback 함수를 사용하게 되므로 느리다.!! 정책 함수를 인라인으로 만들어도 인라인 치환 될 수 없다.  

```c++
#include <iostream>
#include <algorithm>
using namespace std;

void Sort(int *x, int n, bool(*cmp)(int, int)) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            //if (x[i] > x[j]) {
            if (cmp(x[i], x[j])) {
                swap(x[i], x[j]);
            }
        }
    }
}

// Sort() 사용자는 비교 함수를 전달해야 한다.
inline bool cmp1(int a, int b) { return a < b; }
inline bool cmp2(int a, int b) { return a > b; }

void Display(int *x, int n) {
    for (int i = 0; i < n; i++) {
        cout << x[i] << " ";
    }
    cout << endl;
}

int main() {
    int x[10] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10};

    Sort(x, 10, cmp1);
    Display(x, 10);

    Sort(x, 10, cmp2);
    Display(x, 10);

    return 0;
}
```

## 3. ( ) 연산자를 재정의 한 클래스 - 함수 객체
( ) 연산자만 재정의 하면 함수 처럼 사용가능 합니다.
    
```c++
#include <iostream>
using namespace std;

struct Plus {
    int operator()(int a, int b) {
        return a + b;
    }
};

int main() {
    Plus p;
    int n = p(1, 2);
    cout << n << endl;
    return 0;
}
```

## 4.  왜 함수 객체를 사용하는가 ? - 아주 중요합니다.
1. 일반함수는 자신만의 타입이 없다. - 반드시 이해 해야 합니다. signature가 동일한 함수는 같은 타입이다.
2. 함수 객체는 자신만의 타입이 있다. signature가 동일해도 모든 함수객체를 다른 타입이다.

```c++
#include <iostream>
using namespace std;

struct less {
    inline bool operator()(int a, int b) { return a < b; }
};

struct greater {
    inline bool operator()(int a, int b) { return a > b; }
};

// 아래 함수는 cmp()가 인라인 치환되지만 greater로 교체할 수 없다.
// void Sort(int *x, int n, less cmp) {

// 정책교체가 가능하고 정책이 인라인 치환되는 함수
// "템플릿 + 함수객체"를 사용한 기술.!
template<typename T>
void Sort(int *x, int n, T cmp) {
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            if (cmp(x[i], x[j])) { // 인라인 치환 가능
                swap(x[i], x[j]);
            }
        }
    }
}

int main() {
    int x[10] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10};

    less f1;
    greater f2;

    Sort(x, 10, f1);
    Sort(x, 10, f2);
}
```

## 5. 정책 표현 
1. 비교 정책으로 일반 함수를 사용하는 경우  
	* 장점 : 정책을 여러번 교체 해도 sort() 기계어는 한개이다. 메모리 사용량 감소  
	* 단점 : 정책이 인라인 치환될수 없기 때문에 성능저하가 있다.
2. 비교 정책으로 함수객체를 사용하는 경우
	* 장점 : 정책이 인라인 치환 되므로 속도가 빠르다
	* 단점 : 정책을 교체한 횟수 만큼의 sort() 함수 생성, 코드 메모리 증가
3. c++11/14 람다 표현식(lambda experssion)
	* 함수 인자로 함수의 구현(코드)를 전달하는 기술
	* [  ] : lambda introducer

```c++
#include <iostream>
#include <algorithm> // 정책변경이 가능한 sort()가 여기 있다.
using namespace std;

// Sort() 사용자는 비교 함수를 전달해야 한다.
inline bool cmp1(int a, int b) { return a < b; }
inline bool cmp2(int a, int b) { return a > b; }

int main() {
    int x[10] = {1, 3, 5, 7, 9, 2, 4, 6, 8, 10};
    
    // sort의 모든 인자는 템플릿 입니다.    
    sort(x, x + 10, cmp1);  // sort(int*, int*, bool(*)(int, int)) 함수 생성
    sort(x, x + 10, cmp2);  // sort(int*, int*, bool(*)(int, int)) 함수 생성
    
    less<int> f1;
    greater<int> f2;
    sort(x, x + 10, f1);  // sort(int*, int*, less) 함수 생성
    sort(x, x + 10, f2);  // sort(int*, int*, greater) 함수 생성

    sort(x, x + 10, [](int a, int b) { return a < b; });
    return 0;
}
```

## 6. 람다 표현식의 정체!!
```class closure_object``` 를 이용해서 컴파일러는 사용합니다.

```c++
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    int x[10];
    sort(x, x + 10, [](int a, int b) { return a < b; });
    return 0;
}
```

```sort(x, x + 10, [](int a, int b) { return a < b; });```
코드는 컴파일러는 다음과 같이 코드를 번역합니다.

```c++
class closure_object {
public:
    // 함수 객체의 핵심 ()연산자
    inline bool operator()(int a, int b) const {
	return a < b;
    }
};

sort(x, x + 10, closure_object());
```

## 7. lambda and type
같은 람다표현식이라도 다른 타입입니다. 각각의 closure_object 를 생성하기 때문에 다르게 처리됩니다.

```c++
#include <iostream>
using namespace std;

int main() {
    // 람다는 auto에 담을수 있습니다.
    auto f1 = [](int a, int b) { return a + b; };
    auto f2 = [](int a, int b) { return a + b; };
    
    // f1과 f2는 같은 타입일까요? 다른 타입 일까요?
    
    // RTTI 기술로 컴파일러가 만들어준 함수객체 클래스의 이름도 확인 가능합니다.
    cout << typeid(f1).name() << endl;
    cout << typeid(f2).name() << endl;
    
    return 0;
}
```

## 8. 람다를 인자로 받는 방법
람다는 3가지 형태 변수에 담을 수 있습니다.  
```auto f1 = [](int a, int b) { return a + b; };``` // inline 치환이 가능하다. 유일하게  
```int(*f2)(int, int) = [](int a, int b) { return a + b; };```  
```function<int(int, int)> f3 = [](int a, int b) { return a + b; };```  

```c++
#include <iostream>
#include <functional>
using namespace std;

// 람다를 인자로 받는 방법 - 아래 2개 가능하지만 인라인 치환 안됨
// void foo(int(*f2)(int, int)) { }
// void foo(function<int(int, int)> f3) { }

// auto는 절대 함수 인자가 될수 없다. 타입을 모르기 때문이다.
// void foo(auto f1) { } 

// 인라인 치환 가능하게 람다를 받는 유일한 방법 : template
template<typename T>
void foo(T f) {
    f(1, 2);
}

int main() {
    auto f1 = [](int a, int b) { return a + b; };
    int(*f2)(int, int) = [](int a, int b) { return a + b; };
    function<int(int, int)> f3 = [](int a, int b) { return a + b; };

    cout << f1(1, 2) << endl;
    cout << f2(1, 2) << endl;
    cout << f3(1, 2) << endl;

    foo([](int a, int b) { return a + b; });
    return 0;
}
```

## 9. 람다와 리턴타입
리턴 타입의 인자 ( ) 뒤에 적는 문법 - trailing return

```c++
#include <iostream>
using namespace std;

int main() {
    auto f1 = [](int a, int b) { return a + b; };
    auto f2 = [](int a, int b) { return a > b ? a : b; };

    // 리턴 타입의 인자 () 뒤에 적는 문법 - trailing return
    auto f3 = [](int a, int b) -> double { return a > b ? a : 3.0; };

    return 0;
}
```

## 10. 변수 캡쳐
[  ] : lambda introducer 안에 사용 할 변수를 추가한다.  
```[a], [a,b,c], [=] ``` 과 같이 적으면 된다. 여기서 '='은 모든 지역 변수를 의미한다. 

```c++
#include <iostream>

using namespace std;

int main() {
    int v1 = 10;
    int v2 = 20;

    // 지역변수 캡쳐
    auto f = [v1](int a, int b) { return a + b + v1; };
    auto f = [v1, v2](int a, int b) { return a + b + v1 + v2; };
    auto f = [=](int a, int b) { return a + b + v1; }; // 모든 지역 변수 

    // 참조에 의한 캡쳐
    auto f = [&v1](int a, int b) { v1 = 0; return a + b + v1; };
    auto f = [&](int a, int b) { v1 = 0; return a + b + v1; }; // 모든 지역 변수 참조형으로

    cout << v1 << endl;
    cout << f(1, 2) << endl;
    cout << v1 << endl;
    return 0;
}
```

## 11. 변수 캡쳐의 모습 
closure_object의 생성자를 이용해서 변수를 캡쳐한다.

```c++
int v1 = 10;
auto f = [v1](int a, int b) { return a + b + v1; };
```
위의 코드를 풀어서 보겠다.

```c++
class closure_object {
    int value1;
public :
    closure_object(int v1) : value1(v1) { }
    inline int operator()(int a, int b) const {
        return a + b + value1;
    }
};
auto f = closure_object(v1); // 지역변수를 값으로 전달한다.
```

### 참조형도 비슷하게 진행이 된다.

```c++
auto f = [&v1](int a, int b) { return a + b + v1; };
```

위의 코드를 풀어서 보겠다.

```c++
class closure_object {
    int& value1;
public :
    closure_object(int& v1) : value1(v1) { }
    inline int operator()(int a, int b) const {
        return a + b + value1;
    }
};
auto f = closure_object(v1); // 지역변수를 값으로 전달한다.
```

## 12. 멤버 변수 캡쳐
this_call을 잘 생각해야한다. 람다에서 멤버 변수를 캡쳐하기 위해서는 ```this```를 캡쳐하게 하면 된다.

```c++
class Test {
    int base;
public:
    Test(int n) : base(n) { }

    void foo() {
        // 멤버 변수인 base를 캡쳐하려면?
        auto f = [base]() { cout << base << endl; };  // error
    }
};

//다음과 같은 코드로 수정을 해야한다. 
Test::foo() {
	// 참조 캡쳐는 아니지만 포인터를 값으로 했으므로 변경 가능.
	auto f = [this]() {
		base = 200;
		cout << base << endl;
		};  // ok
	f();
}
```

## 13. ㅁ

```c++
#include <iostream>

using namespace std;

int main() {
    // auto f1 = []() { cout << "f1" << endl; };

    // 인자가 없는 람다는 ()의 생략이 가능하다.
    // Nullary lambda expression
    auto f1 = [] { cout << "f1" << endl; };

    f1();

    return 0;
}
```

## 14. ㅁ

```c++
#include <iostream>

using namespace std;

int main() {
    int v = 10;

    auto f1 = [&v]() { v = 0; };    // 참조로 캡쳐
    // auto f2 = [v]() { v = 0; };     // 값으로 캡쳐 // error : v 변경 불가능

    // mutable lambda
    auto f3 = [v]() mutable { v = 200; }; // 값으로 캡쳐 - 복사본을 변경가능, 원본은 그대로

    f1();
    cout << v << endl;

    // f2();

    f3();
    cout << v << endl;

    // 숙제 2. f1, f2, f3이 만들어내는 클로져 클래스를 예측해보세요.
    // f2가 왜 애러가 나는지 ?
    // f1은 왜 값이 바뀌는지 ?
    // f3은 복사본을 변경해도 에러가 나지 않게 하기 위해 어떻게 할지 직접 클래스를 만들어 보세요.

    return 0;
}
```

