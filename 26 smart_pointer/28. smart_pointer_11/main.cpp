#include <iostream>

using namespace std;

// 부모 포인터인 상태로 delete 하면 부모의 소멸자만 호출됩니다.
// 자식 소멸자를 호출되게 하려면 소멸자 자체가 가상함수 이어야합니다.
// 결론 : 모든 부모의 소멸자는 가상함수 이어야 한다.

//class RefBase {
//    int mCount;
//public:
//    RefBase() : mCount(0) { }
//    virtual ~RefBase() { }
//    void incStrong() { ++mCount; }
//    void decStrong() { // decStrong(RefBase* this)
//        if ((--mCount) == 0) {
//            delete this;
//        }
//    }
//};

// 가상함수가 포함되면 가상함수 테이블을 가리키는 포인터가 추가 된다.
// 이러한 점이 너무 안좋기 때문에 바꿔야 한다.

template<typename T>
class LigthRefBase {
    int mCount;
public:
    inline LigthRefBase() : mCount(0) { }
    inline ~LigthRefBase() { }
    inline void incStrong() { ++mCount; }
    inline void decStrong() { // decStrong(RefBase* this)
        if ((--mCount) == 0) {
            delete static_cast<T*>(this);
        }
    }
};

template<typename T>
class sp {
    T* obj;
public :
    sp(T* p = 0) : obj(p) {
        if (obj != 0) {
            obj->incStrong();
        }
    }
    sp(const sp& p) : obj(p.obj) {
        if (obj != 0) {
            obj->incStrong();
        }
    }
    ~sp() {
        if (obj) {
            obj->decStrong();
        }
    }
    T* operator->() { return obj; }
    T& operator*() { return *obj; }
};

class Truck : public LigthRefBase<Truck> {
public:
    ~Truck() { cout << "Truck" << endl; }
};

int main() {
    sp<Truck> p = new Truck;
    return 0;
}