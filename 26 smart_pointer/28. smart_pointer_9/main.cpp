#include <iostream>
#include <vector>
#include <list>

using namespace std;

// 객체 관리의 어려움
class Car { };

vector<Car*> v;
list<Car*> s;

void foo() {
    Car* p = new Car;
    p->incStrong();

    v.push_back(p);
    p->incStrong();

    s.push_back(p);
    p->incStrong();

    // 여기서 더이상 p가 필요없다. 제거해도 될까?
    delete p;
    p->decStrong();
}

int main() {
    foo();
    return 0;
}