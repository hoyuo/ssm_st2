#include <iostream>

using namespace std;

// 참조계수를 객체안에 포함시키기
class Car {
    //Car 고유 멤버들
    int mCount;

public:
    Car() : mCount(0) {}
    ~Car() {cout << "~Car" << endl;}

    void incStrong() {
        ++mCount;
    }
    void decStrong() {
        if((--mCount)==0) {
            delete this;
        }
    }
};

int main() {
    Car* p1 = new Car;
    p1->incStrong(); //  규칙 1. 객체 생성지 참조 계수 증가

    Car* p2 = p1;
    p2->incStrong(); // 규칙 2. 객체 파괴시 참조 계수 즉가

    p2->decStrong();
    p1->decStrong();
    return 0;
}