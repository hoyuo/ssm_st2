#include <iostream>

using namespace std;

// 참조계수를 조작하는 함수는 반드시 상수 함수 이어야 한다.
template<typename T>
class LightRefBase {
    mutable int mCount;
public:
    inline LightRefBase() : mCount(0) { }
    inline ~LightRefBase() { }
    inline void incStrong() const { ++mCount; }
    inline void decStrong() const { // decStrong(const RefBase* this)
        // this : const RefBase* 입니다
        // static cast는 상수성을 제거할 수 없습니다.

        // RefBase*       -> T*       : ok
        // const RefBase* -> const T* : ok
        // const RefBase* -> T*       : error

        if ((--mCount) == 0) {
            delete static_cast<const T*>(this);
//            delete static_cast<T*>(const_cast<LightRefBase*>(this));
        }
    }
};

template<typename T>
class sp {
    T* obj;
public :
    sp(T* p = 0) : obj(p) {
        if (obj != 0) {
            obj->incStrong();
        }
    }
    sp(const sp& p) : obj(p.obj) {
        if (obj != 0) {
            obj->incStrong();
        }
    }
    ~sp() {
        if (obj) {
            obj->decStrong();
        }
    }
    T* operator->() { return obj; }
    T& operator*() { return *obj; }
};

class Truck : public LightRefBase<Truck> {
public:
    ~Truck() { cout << "Truck" << endl; }
};

class Test {
    int x;
public :
    void foo() { //void foo(Test* this)
        // this 자체는 const이지만 this가 가리키는 곳은 const 아님
        this->x = 10;
//        this = 0; // error
    }

    void goo() const { // void goo(const Test* const this)
        // this도 const, this가 가리키는 곳도 const
        this->x = 10; // error
    }
};

int main() {
//    const Truck t;
//    t.incStrong(); // 상수 객체도 참조계수 관리는 할 수 있어야 한다.

    sp<Truck> p = new Truck;

    return 0;
}