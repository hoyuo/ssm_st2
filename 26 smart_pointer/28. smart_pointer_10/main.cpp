// 스마트 포인트 10
// 스마트 포인터가 참조계수를 관리하는 2가지 방식

#include <iostream>
#include <__mutex_base>
using namespace std;
// 1. 참조계수를 위한 별도의 메모리 할당
//    shared_ptr<> : c++ 표준, 모든 타입에 대해 사용 가능
shared_ptr<int> p(new int); //모든 타입에 대해 사용 가능

// 2. 객체 내부에 참조계수를 포함
//    C++ 표준이 아니라 각 라이브러리가 자체적으로 제공하는 기술들
//    (안드로이드 RefBase, sp)
//    (Poco,  )
sp<int> p = new int; // 될까요? error. sp를 사용하려면 RefBase 자식만 된다.


// www.pocoproject.com
// \poco\poco\Foundation\include\Poco

// 다양한 헤더 중에서 참조계수를 책임지는 클래스가 있는 헤더를 찾아 보세요
// RefCountedObject.h