#include <iostream>

using namespace std;

class RefBase {
    int mCount;
public:
    RefBase() : mCount(0) { }
    virtual ~RefBase() { }

    void incStrong() { ++mCount; }
    void decStrong() {
        if ((--mCount) == 0) {
            delete this;
        }
    }
};

template<typename T>
class sp {
    T* obj;
public :
    sp(T* p = 0) : obj(p) {
        if (obj != 0) {
            obj->incStrong();
        }
    }
    sp(const sp& p) : obj(p.obj) {
        if (obj != 0) {
            obj->incStrong();
        }
    }
    ~sp() {
        if (obj) {
            obj->decStrong();
        }
    }

    //스마트포인터의 기본 -> 와 *
    T* operator->() { return obj; }
    T& operator*() { return *obj; }
};

// sp를 사용하려면 반드시 incStrong와 decStrong은 있어야한다.
// sp를 사용하려면 RefBase의 파생 클래스로 만들어야 한다
class Truck : public RefBase {
public:
    ~Truck() { cout << "Truck" << endl; }
};

int main() {
    sp<Truck> p = new Truck;
    return 0;
}