#include <iostream>
#include <string>

using namespace std;

// 변환의 장단점

class OFile {
    FILE* file;

public :
//    explicit : 인자가 한개인 생성자가 암시적 변환을 일으키는 것을 막는다.
//    단, 명시적 변환은 허용한다.

    explicit OFile(const char* name, const char* mode = "wt") {
        file = fopen(name, mode);
    }

    ~OFile() {
        fclose(file);
    }

    // OFile => FILE*로의 변환을 허용한다.
    operator FILE*() { return file; }
};

//int main() {
////    FILE* f = fopen("a.txt", "wt");
//    OFile f("a.txt");
//
////    파일 IO 작업
//    fprintf(f, "n=%d", 10);
//    fputs("hello", f);
//
//    string s1 = "hello"; // String 클래스
//
//    char s2[10];
//
//    strcpy(s2, s1);
//
//    return 0;
//}

void foo(OFile f) { }

int main() {
    OFile f("a.txt");
    foo(f); // ok.. 당연하다!!

    foo("hello"); // ?? 컴파일 에러가 나지 않는다?? 왜?
//    const char* => OFile 로 변환되면 ok.
//    생성자에 explicit 붙이면 error가 발생한다.

    foo(static_cast<OFile>("hello")); // ok.. 명시적 변환
}