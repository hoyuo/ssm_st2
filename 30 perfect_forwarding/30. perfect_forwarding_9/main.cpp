// perfect forwarding
#include <iostream>

using namespace std;

void foo(int a) {
    cout << "foo" << endl;
}

void goo(int& a) {
    cout << "goo" << endl;
    a = 20;
}

template<typename F, typename T>
void HowLong(F f, T&& a) {
    f(a);
    //f(forward<T&&>(a));
};

int main() {
    int n = 0;

    HowLong(foo, 0); // 0 : rValue
    HowLong(goo, n); // n : lValue

    cout << n << endl; // 20;

    return 0;
}