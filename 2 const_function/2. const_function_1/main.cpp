#include <iostream>

using namespace std;

class Point {
public:
    int x, y;

    Point(int a = 0, int b = 0) : x(a), y(b) { }

    void set(int a, int b) {
        x = a;
        y = b;
    }

    void print() const {
        //x = 10;//error, 상수 함수 안에서는 멤버 값을 변경할 수 없다.
        cout << x << ", " << y << endl;
    }
};

int main() {
    const Point p(0, 0);
    //p.x = 10;// 상수 이기때문에 애러이다.
    //p.set(10, 10); //? error : 객체를 상수화를 하였기 때문에 상수함수만 가능하다.

    p.print();
    return 0;
}