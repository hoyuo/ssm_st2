#include <iostream>

using namespace std;
struct Cache {
    char data[32];
    bool valid;
};

class Point {
    int x, y;
    Cache *pCache;

public :
    Point(int a = 0, int b = 0) : x(a), y(b) {
        pCache = new Cache;
        pCache->valid = false;
    }

    char *toString() const {
        if (pCache->valid == false) {
            sprintf(pCache->data, "%d, %d", x, y);
            pCache->valid = true;
        }
        return pCache->data;
    }

    ~Point() {
        delete pCache;
    }
};

int main() {
    Point p(1, 2);
    cout << p.toString() << endl;
    cout << p.toString() << endl;
    return 0;
}