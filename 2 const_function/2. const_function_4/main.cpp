#include <iostream>

using namespace std;

class Point {
    int x, y;
    mutable char cache[32];
    mutable bool cache_valid; // 상수 함수에서도 변경 가능한 멤버 data

public :
    Point(int a = 0, int b = 0) : x(a), y(b), cache_valid(false) { }

    //객체의 상태를 문자열로 반환하는 함수 : java, C#에 있는 개념
    char *toString() const {
        //static char s[32];
        if (cache_valid == false) {
            sprintf(cache, "%d, %d", x, y);
            cache_valid = true;
        }
        return cache;
    }
};

int main() {
    Point p(1, 2);
    cout << p.toString() << endl;
    cout << p.toString() << endl;
    return 0;
}