#include <iostream>

using namespace std;

class Rect {
    int x, y, w, h;

public:
    int getArea() const { return w * h; }
};

//C++ 기본 문법 : call by value 대신 const & 가 좋다.
void foo(const Rect &r) {
    int n = r.getArea();
}

int main() {
    Rect r; //초기화 하였다고 가정하고

    int n = r.getArea(); // ok

    foo(r);
}