## 9. function<>
모든 함수의 주소를 담을 수 있는 도구  
c,c++       : 문법적으로는 없다.  
c#          : delegate 라는 문법  
objective-c : Selector 라는 문법  
  
c++11 : function<> 모든 함수의 주소를 담을 수 있다.  