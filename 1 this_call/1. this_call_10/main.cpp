#include <iostream>
#include <functional>

using namespace std;

void foo() { cout << "foo" << endl; }

void goo(int a) { cout << "goo : " << a << endl; }

void hoo(int a, int b) { cout << "hoo : " << a << ", " << b << endl; }

class Dialog {
public:
    void Close() {
        cout << "Dialog Close" << endl;
    }
};

int main() {
    function<void()> f = &foo;
    //function<리턴값(파라메터)>
    f(); // ok..foo() 호출

    f = bind(&goo, 5);// 인자를 고정
    f();//goo(5)

    f = bind(&hoo, 1, 2);
    f();//hoo(1,2)

    Dialog dlg;
    f = bind(&Dialog::Close, dlg);// 객체를 고정
    f();//dlg.Close()

    return 0;
}