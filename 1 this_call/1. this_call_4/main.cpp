//VC로만 가능

#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

// C의 스레드 개념을 c++로 캡슐화 해 봅시다.
// 아래 클래스를 라이브러리 내부 클래스라고 생각합니다.
class Thread {
public:
    void Create() {
        CreateThread(0, 0, _threadMain, this, 0, 0); // !!
    }

    // 스레드 함수
    // 핵심 1. C의 callback 함수는 객체 지향으로 디자인 될때 static 멤버함수가 되어야 한다.
    // 핵심 2. static 멤버에는 this가 없으므로 가상함수나 멤버 data에 접근할 수 없다. 다양한 기법으로 this를 사용할 수 있게 하는것이 편리하다.
    static DWORD __stdcall _threadMain(void *p) {
        // 다시 가상함수 호출
        // threadMain();   // this->threadMain();
        // 즉 threadMain(this)가 되어야 한다.

        // 그러나 threadMain()으로는 사용할수가 없게 된다.
        // 방법은 p가 this이므로 캐스팅해서 사용합니다
        Thread* self = static_cast<Thread*>(p);
        self->threadMAin(); // 결국 threadMain(self)
        return 0;
    }

    virtual void threadMain() { // threadMain(Thread* this)
    }
};

// 아래 클래스가 라이브러리 사용자 클래스 입니다.
class MyHtread : public Thread {
public :
    virtual void threadMain() {
        cout << "MyThread" << endl;
    }
};

int main() {
    MyHtread t;
    t.Create();
    return 0;
}