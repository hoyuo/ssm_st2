#include <iostream>

using namespace std;

class Dialog {
public:
    void Close() {
        cout << "Dialog Close" << endl;
    }
};

template<typename T>
class Button {
    //void(*handler)();
    void(T::*handler)();

    //멤버 함수를 담기 위해서 만든다.
    T *member;
public:
    void setHandler(void(T::*f)()) {
        handler = f;
    }

    void click() {
        // 버튼이 눌렸다는 사실을 외부에 전달합니다.
        // 흔히 "객체가 외부에 이벤트를 발생한다."라고 표현!
        (member->*handler)();
    }
};

void Btn1Handler() { cout << "버튼 1 클릭" << endl; }

int main() {
    Button<Dialog> b1;
    b1.setHandler(&Dialog::Close);
    b1.click(); // 사용자가 버튼을 클릭하면 이함수가 호출된다고 가정합니다.

    return 0;
}