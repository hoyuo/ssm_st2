#include <iostream>

using namespace std;

class X {
public:
    int x;

    void fx() { cout << this << endl; }
};

class Y {
public:
    int y;

    void fy() { cout << this << endl; }
};

class C : public X, public Y {
public:
    int c;
};

int main() {
    C c;

    cout << &c << endl; // 100번지 라고 할 때

    c.fx(); // 100
    c.fy(); // 104

    void (C::*f)();

    //f = &C::fx; // { fx 주소, 0 }
    //(c.*f)();// 결과 ? 100 나와야 한다.
    //f(&c)

    f = &C::fy; // { fy 주소, sizeof(X) 즉, 4 }
    (c.*f)(); //f(&c)
    // f.함수주소(&c + f.this_offset)

    cout << sizeof(f) << endl;

    return 0;
}