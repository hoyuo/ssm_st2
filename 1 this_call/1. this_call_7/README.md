## 6. 상속과 포인터 
우리가 예상하는 것보다 컴파일러는 주소 값을 잘 찾아서 간다. 컴파일러가 어떻게 이 과정을 찾는지는 모른다.  

>모든 함수 포인터는 4바이트라고 생각했지만 일반적은 함수 포인터만 4바이트
>다중 상속을 하는 경우 포인터의 주소는 8바이트 인다. { 함수주소, this offset }