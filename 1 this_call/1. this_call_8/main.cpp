#include <iostream>

using namespace std;

class Base {
public:
    virtual void goo() { cout << "Base foo" << endl; }

    virtual void foo() = 0;// { cout << "Base foo" << endl; }
};

class Dervied : public Base {
public:
    virtual void foo() { cout << "Derived foo" << endl; }
};

int main() {
    // 가상함수의 경우는 가상함수 테이블의 인덱스 번호가 넘어온다.
    // 즉,  가상함수 table의 인덱스, 가상함수의 순서가 나오게 된다.
    // g++, xcode : 0, 1, 2, 3 등의 숫자가 나오게 됩니다.
    // vc++ : 주소 비슷하게 나오는데... 그 주소를 따라가면 index가 있습니다.

    void (Base::*f)() = &Base::foo;// 잘 생각해 보자.

    //cout << f << endl;
    printf("%d\n", f);//foo -> 9
    //goo -> 1

    Base *p = new Dervied;

    (p->*f)(); // ?
}
