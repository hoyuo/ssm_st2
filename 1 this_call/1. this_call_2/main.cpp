#include <iostream>

using namespace std;

/*
 * 1. 일반 함수 포인터에 멤버 함수의 주소를 담을 수 없다.
 *    this때문에..!!
 *
 * 2. 일반 함수 포인터에 static 멤버 함수의 주소를 담을 수 있다.
 *
 * 3. 멤버 함수 포인터를 만들고 사용하는 방법
 *
 *    void(Dialog::*f3)() = &Dialog::Close;
 *
 *    Dialog dlg;
 *    (dlg.*f3)();
 */

class Dialog {
public:
    void Close() {
        cout << "Dialog Close" << endl;
    }
};

void foo() {
    cout << "foo" << endl;
}

int main() {
    void(*f1)() = &foo;//OK

    //void(* f2) () = &Dialog::Close;// 될까요 ? 잘 생각해 보세요
    // error

    //멤버 함수 포인터를 만드는 법
    void(Dialog::*f3)() = &Dialog::Close; //OK...외우자

    // f3(); //될까요??
    // error

    Dialog dlg;
    // dlg.f3(); // ok 결국 dlg.Close() 즉 f3(&dlg)
    // 그런데. 이경우 컴파일러는 f3이라는 멤버를 찾게된다.
    // 그래서 error

    (dlg.*f3)();    // f3는 함수포인터이므로 *f3하면 함수가 된다.
    // .* 연산자 우선순위를 호출함수()보다 높여야 한다.

    Dialog *pDlg = &dlg;

    //pDlg와 f3를 사용해서 Close를 호출해 보세요.

    ((*pDlg).*f3)();
    (pDlg->*f3)();

    return 0;
}