#include <iostream>

using namespace std;

class X {
public:
    int x;
};

class Y {
public:
    int y;
};

class C : public X, public Y {
public:
    int c;
};

int main() {
    C c;

    cout << &c << endl; //100번지 라고 할 때

    X *pX = &c;
    Y *pY = &c;

    cout << pX << endl; // ? 100
    cout << pY << endl; // ? 104

    return 0;
}