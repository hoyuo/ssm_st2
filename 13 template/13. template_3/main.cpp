#include <iostream>

using namespace std;

template <typename T>
void print(const T& d)
{
    cout << T::N << endl;
}

template <typename T, typename U>
struct Duo
{
    T value1;
    U value2;

    enum
    {
        N = 2
    };
};

// Duo의 2번째 인자가 다시 Duo일때(Recursive)를 위한 부분전문화
// 부분 전문화 할때 템플릿 인자 갯수가 바뀌어도 됩니다.
template <typename A, typename B, typename C>
struct Duo<A, Duo<B, C>>
{
    A         value1;
    Duo<B, C> value2;
    enum
    {
        N = 1 + Duo<B, C>::N
    };
};

template <typename A, typename B, typename C>
struct Duo<Duo<A, B>, C>
{
    Duo<A, B> value1;
    C         value2;
    enum
    {
        N = 1 + Duo<A, B>::N
    };
};

template <typename A, typename B, typename C, typename D>
struct Duo<Duo<A, B>, Duo<C, D>>
{
    Duo<A, B> value1;
    Duo<C, D> value2;
    enum
    {
        N = Duo<A, B>::N + Duo<C, D>::N
    };
};

//int main()
//{
//    Duo<int, int>                     d2;
//    Duo<int, Duo<int, int>>           d3;
//    Duo<int, Duo<int, Duo<int, int>>> d4;
//    print(d2);  // 2
//    print(d3);  // 3
//    print(d4);  // 4
//    return 0;
//}

//typedef Duo<int, int> Point;

//int main()
//{
//    Duo<Duo<int, int>, int>           d3;
//    Duo<Duo<Duo<int, int>, int>, int> d4;
//
//    print(d3);  // 3
//    print(d4);  // 4
//}

// 1, 2 모든 인자가 Duo 일 때

int main()
{
    Duo<Duo<int, int>, Duo<int, int>> d4;
    Duo<Duo<Duo<int, int>, Duo<int, int>>, Duo<Duo<int, int>, Duo<int, int>>> d5;

    print(d4);  // 4
    print(d5);  // 8
}