#include <iostream>

using namespace std;

// tuple은 c++표준에 이미 있습니다.
// vc++2010 ~ 2012 : 10개까지 가능, 우리가 만든 코드와 유사한 기술
// vc++2013 ~ 2015 : c++11의 가변인자 기술... 이론상 무한히 가능...

int main()
{
    int x[10];  // 같은 타입 10개

    tuple<int, double, long> t3(1, 3.3, 2);

    // tuple 에서 값 꺼네기
    cout << get<1>(t3) << endl;

    return 0;
}