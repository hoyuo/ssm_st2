cmake_minimum_required(VERSION 3.2)
project(13__template_2)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp)
add_executable(13__template_2 ${SOURCE_FILES})