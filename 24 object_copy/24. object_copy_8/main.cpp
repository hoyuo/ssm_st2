#include <iostream>

using namespace std;

class Cursor {
private :
    Cursor() { }
public :
    static Cursor* pInstance;

    static Cursor& getInstance() {
        if (pInstance == 0) {
            pInstance = new Cursor;
            return *pInstance;
        }
        return *pInstance;
    }

    // 싱글톤은 복사와 대입을 제거해야 한다.
    Cursor(const Cursor&) = delete;
    Cursor& operator=(const Cursor&) = delete;
};

Cursor* Cursor::pInstance = 0;

int main() {
    Cursor& c1 = Cursor::getInstance();
    Cursor c2 = c1;
    return 0;
}