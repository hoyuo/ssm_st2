#include <iostream>

using namespace std;
int main() {
    int n = 10;

    // 어느 메모리가 상수 인가?
    const int* p1 = &n; // p1을 따라가면 const가 있다
    int* const p2 = &n; // p2을 따라가면 const가 아니다.
    // 하지만 p2 자체가 const이다.

//    int n2 = 20;
//    p2 = &n2; // error
//    *p2 = 20; // ok

    int const* p3 = &n; // p1 ? p2 와 동일 -> p1과 동일
//    const int* p3 = &n; // 위 아래는 같은 표현이다
    return 0;
}

// this의 상수성
class Point {
    int x, y;
public :
    void Set() { // void Set(Point* const this)
        x = 0; // this->x = 0;
        y = 0; // this->y = 0;

//        this = 0; // error. this 포인터 자체는 상수이다.

    }
    void Print() const { // void Print (const Point* const this)
        x = 10; // this->x = 10; // error
    }
};