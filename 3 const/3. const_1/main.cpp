#include <iostream>

using namespace std;

int main() {
    //const int c = 10; -- 1//컴파일 시간 상수, 메크로 처럼 동작한다.

    int n = 10;// -- 2
    const int c = n; //runtime 상수, read only 상수
    // 실행시간에 변경 불가능하다는 의미

    //int *p = &c; // ?? 상수의 주소를 int*에 담기 // error

    int *p = (int *) &c; // ok... 또는 c++ const_cast<>도 ok...!

    *p = 20;
    // 1  2
    cout << c << endl;  //10 20
    cout << *p << endl; //20 20
    return 0;
}