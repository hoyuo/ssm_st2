# const
## 1. const(상수)
compile 상수 : 컴파일 시간 상수, 매크로처럼 동작   
run-time 상수 : read only 상수

```c++
#include <iostream>

using namespace std;

int main() {
    //const int c = 10; -- 1//컴파일 시간 상수, 메크로 처럼 동작한다.

    int n = 10;// -- 2
    const int c = n; //runtime 상수, read only 상수
    // 실행시간에 변경 불가능하다는 의미

    //int *p = &c; // ?? 상수의 주소를 int*에 담기 // error

    int *p = (int *) &c; // ok... 또는 c++ const_cast<>도 ok...!

    *p = 20;
    				   // 1  2
    cout << c << endl;  //10 20
    cout << *p << endl; //20 20
    return 0;
}
```

## 2. 배열선언과 상수 
c89 : 배열의 크기는 컴파일 시간 상수만 된다.  
c99 : 배열의 크기는 변수도 가능하다. gcc 지원, vc++ 지원 안함

```c++
#include <iostream>

using namespace std;

int main() {
    int x1[3]; // ok

    int s2 = 3;
    int x2[s2]; // gcc : ok, vs : error

    const int s3 = 3;
    int x3[s3]; // ok

    const int s4[] = {1, 2, 3};
    int x4[s4[1]]; // gcc : ok, vs : error
    //int x4[ *(s4+1) ] 결국 배열(s4)은 포인터 이므로 런타임 상수이다.
    return 0;
}

void foo(const int s) {
    int x[s]; // ? 여기서 들어오는 s는 런타임 상수이다 gcc : ok, vs : error
}
```

## 2. 배열선언과 상수
c++11 :컴파일 상수를 위한 새로운 키워드 도입 ```constexpr```

```c++
#include <iostream>

using namespace std;

constexpr int foo(int a, int b) {
    //c++14 부터는 인자 앞에도 constexpr도 가능하다.
    return a + b;
}

int main() {
    int n = 10;

    // C++ 98/03 스타일 : 1998년 표준화 되고 2003년 버그수정(개정판) C++
    const int c1 = 10; //ok
    const int c2 = n; // ok

    // C++11/14 : 2011년 2차 표준화 14년 버그수정(개정판) C++
    constexpr int c3 = 10; // ok
    //constexpr int c4 = n; // error. 컴파일 상수가 아니다.

    // 배열크기로 함수도 사용가능
    // 단, 함수리턴값은 constexpr이어야 한다.
    int x[foo(1, 2)];
    return 0;
}
```