#include <iostream>

using namespace std;

template<typename T>
class Base {
public :
//    void test() {foo();}
    virtual void foo(const T& a) {
        cout << "1" << endl;
    }
};

typedef int* PINT

class Derivec : public Base<int*> {
public:
    void foo(const int* a) {
        cout << "2" << endl;
    }
};

int main() {
    Base<int> p = new Derivec;
    p->foo(200);
}