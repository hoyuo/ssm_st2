#include <iostream>

using namespace std;

int main() {
    int x1[3]; // ok

    int s2 = 3;
    int x2[s2]; // gcc : ok, vs : error

    const int s3 = 3;
    int x3[s3]; // ok


    const int s4[] = {1, 2, 3};
    int x4[s4[1]]; // gcc : ok, vs : error
    //int x4[ *(s4+1) ] 결국 배열(s4)은 포인터 이므로 런타임 상수이다.
    return 0;
}

void foo(const int s) {
    int x[s]; // ? 여기서 들어오는 s는 런타임 상수이다 gcc : ok, vs : error
}