#include <iostream>

using namespace std;

constexpr int foo(int a, int b) {
    //c++14 부터는 인자 앞에도 constexpr도 가능하다.
    return a + b;
}

int main() {
    int n = 10;

    // C++ 98/03 스타일 : 1998년 표준화 되고 2003년 버그수정(개정판) C++
    const int c1 = 10; //ok
    const int c2 = n; // ok

    // C++11/14 : 2011년 2차 표준화 14년 버그수정(개정판) C++
    constexpr int c3 = 10; // ok
    //constexpr int c4 = n; // error. 컴파일 상수가 아니다.

    // 배열크기로 함수도 사용가능
    // 단, 함수리턴값은 constexpr 이어야 한다.
    int x[foo(1, 2)];
    return 0;
}

