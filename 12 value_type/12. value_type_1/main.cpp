//value_type
#include <iostream>
#include <list>
#include <vector>

using namespace std;

//void print(vector<int>& v)
//{
//    int n = v.front();
//    cout << n << endl;
//}

// 모든 타입에서 지원하기 위해서 템플릿으로 바꾼다.
//template <typename T>
//void print(vector<T>& v)
//{
//    T n = v.front();
//    cout << n << endl;
//}

//template <typename T>
//void print(T& v)
//{
//    // T             => list<double>
//    // T::value_type => double
//    typename T::value_type n = v.front();
//    cout << n << endl;
//}
// 모든 template 기반 컨테이너에는 저장하는 타입이 있습니다.
// 그런데 그 타입을 외부에서(위에 print) 알고 싶을 때가 있습니다.
// 그래서, 아래처럼 만들게 됩니다.

template <typename T>
class list
{
public:
    typedef T value_type;
    //......
};

list<int>::value_type n; // 결국 n은 int 타입이다.

template <typename T>
void print(T& v)
{
    // C++11의 도입은 기존 소스를 보다 간단하게 표현할수 있습니다.
    // 결국 value_type 개념이 없어도 됩니다.
    auto n = v.front();
    cout << n << endl;
}

int main()
{
    list<double> v(10, 3);
    print(v);
    return 0;
}