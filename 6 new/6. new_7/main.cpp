#include <iostream>

using namespace std;

void *operator new(size_t sz) {
    void *p = malloc(sz);

    if (p == 0)
        throw std::bad_alloc(); // 예외 전달

    return p;
}

// 단지, 사용되지 않고 함수 오버로딩을 위한 인자가 필요 할때는 새로운 타입을 설계하는 것이 좋다 - "empty class" 개념
// struct nothrow_t { }; // empty class - 아무 멤버도 없다.

// nothrow_t nothrow;  // sizeof(nothrow) => 1byte 입니다.

void *operator new(size_t sz, nothrow_t n) {
    void *p = malloc(sz);
    return p;
}

//#define nothrow 1

int main() {
    int *p = new(nothrow) int[100];
    return 0;
}