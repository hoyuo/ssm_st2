#include <iostream>

using namespace std;

class Point {
    int x, y;
public:
    Point() { cout << "생성자" << endl; }

    ~Point() { cout << "소멸자" << endl; }
};

int main() {
//    Point *p = new Point;
//    delete p;

    // 생성자 호출없이 메모리 할당만 하는 방법

    Point *p = static_cast<Point *>(operator new(sizeof(Point)));
    operator delete(p);

    return 0;
}