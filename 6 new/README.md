# new

## 1. new
### new의 정확한 동작 방식
1.  operator new() 함수를 사용해서 메모리 할당  
2. 1이 성공하고 객체라면 생성자 호출  
3. 메모리 주소를 해당 타입으로 캐스팅해서 리턴  

```c++
#include <iostream>
using namespace std;

class Point {
    int x, y;
public:
    Point() { cout << "생성자" << endl; }
    ~Point() { cout << "소멸자" << endl; }
};

int main() {
    // 생성자 호출없이 메모리 할당만 하는 방법
    Point *p = static_cast<Point *>(operator new(sizeof(Point)));
    operator delete(p);

    return 0;
}
```

## 2.operator new overloading
함수 오버로딩이 가능합니다. 단, 1번째 인자는 반드시 ```size_t```이어야 합니다.

```c++
#include <iostream>
using namespace std;

void* operator new(size_t sz) {
    cout << "my new" << endl;
    return malloc(sz);
}

// 함수 오버로딩이 가능합니다. 단, 1번째 인자는 반드시 size_t이어야 합니다.
void *operator new(size_t sz, char *s, int n) {
    cout << "my new2" << endl;
    return malloc(sz);
}

void operator delete(void *p) {
    cout << "my delete" << endl;
    free(p);
}

int main() {
    int *p1 = new int; // sizeof(int) 즉, 4가 인자로 전달
    delete p1;

    int *p2 = new("AAA", 3) int; // sizeof(int) 즉, 4가 인자로 전달
    delete p2;

    return 0;
}
```

## 3. 생성자의 명시적 호출
"Placement new"라고 불리는 함수를 통해서 할 수 있습니다.  

```c++
#include <iostream>
using namespace std;

class Point {
    int x, y;
public:
    Point() { cout << "생성자" << endl; }
    ~Point() { cout << "소멸자" << endl; }
};

/* 아래 함수가 어렵습니다. 잘 생각해 보세요
 * 이미 C++ 표준에 아래 함수가 제공됩니다. - 1994년에 나온 개념
 * "Placement new"라고 불리는 함수 입니다.
 * 이미 존재하는 메모리에 생성자만 다시 호출하는 개념입니다.
 * 아래 코드는 구현할 필요가 없습니다. 이미 C++에서 제공하고 있는 함수입니다.
 */
void* operator new(size_t sz, void *p) {
    return p;
}


int main() {
    Point p;
    new Point; // 인자가 한개인 operator new() 호출
    new(&p) Point;
    // 위에 만든 인자 2개인 operator new() 호출
    // 객체 p에 대해서 생성자를 명시적으로 호출한것!

    // p.Point();  // 생성자의 명시적 호출 ? error.
    p.~Point(); // 소멸자의 명시적 호출 ? ok...

    return 0;
}
```

## 3. 생성자의 명시적 호출
생성자의 명시적 호출을 통해서 기본 생성자가 아닌 인자를 받는 생성자 또한 가능 합니다.

```c++
#include <iostream>
using namespace std;

class Point {
    int x, y;
public:
    Point(int a, int b) : x(a), y(b) { }
};

int main() {
    // 1. 힙에 Point 한개를 만들어 보세요
    Point *p1 = new Point(1, 2);

    // 2. 힙에 Point 10개를 만들어 보세요
    Point *p2 = new Point[10];  // 잘못되었다. 기본 생성자만 호출하게 된다.

    Point *p3 = static_cast<Point *>(operator new(sizeof(Point) * 10));
    // 할당된 메모리의 생성자를 호출해서 객체를 초기화 한다.
    for (int i = 0; i < 10; i++) {
        new(&p3[i]) Point(i, i);
    }

    // 이경우 소멸자 호출 및 메모리 해지도 아래 처럼 해야한다.
    for (int i = 9; i > -1; i--) {
        p3[i].~Point(); // 소멸자 명시적 호출
    }
    operator delete(p3);

    // Point를 스택에 10개 만들어 보세요
    Point p4[10]; //? -> 정답이 아니다.

    char buf[sizeof(Point) * 10];
    Point *p4 = reinterpret_cast<Point *>(&buf);
    // p4에 대해 생성자를 명시적으로 호출하고 사용하면된다.
    // 사용후 소멸자도 명시적으로 호출해야 한다.
    for(int i=0; i<10; i++) {
        new (&p4[i]) Point(i, i);
        printf("%p\n", &p4[i]);
    }

    for (int i = 9; i > -1; i--) {
        p4[i].~Point(); // 소멸자 명시적 호출
    }
    
    return 0;
}
```

## 4. 생성자의 명시적 호출 (vector의 메모리 관리) 
STL에서는 생성자와 소멸자의 명시적 호출이 빈번하게 사용된다.  
제일 쉽게 접할 수 있는 vector를 보겠습니다.

```c++
#include <iostream>
#include <vector>

using namespace std;

int main() {
    vector<int> v(10, 3);   // 10개를 3으로 초기화

    cout << v.size() << endl;   // 10
    cout << v.capacity() << endl;   // 실제 메모리 크기 10

    v.resize(7);    // 이 코드의 알고리즘을 생각해 봅시다.
    // 실제로 메모리를 줄이는 것이 아니라 사용량의 숫자만 바꾼다. 
    cout << v.size() << endl;   // 7
    cout << v.capacity() << endl;   // 실제 메모리 크기 10

    v.push_back(5);

    cout << v.size() << endl;   // 8
    cout << v.capacity() << endl;   // 실제 메모리 크기 10

    v.shrink_to_fit();  // 필요없는 공간을 제거해달라

    cout << v.size() << endl;   // 8
    cout << v.capacity() << endl;   // 실제 메모리 크기 8

    vector<int> v2(10, 0);
    v2.push_back(1);

    cout << v2.size() << endl;   // 11
    cout << v2.capacity() << endl;   // 실제 메모리 크기 20

    // vector에 사용자 타입을 저장하는 경우를 생각 해 봅시다.
    class DBConnect {
    };
    vector<DBConnect> v3(10);   // DBConnect는 생성자에서 DB에 접속합니다.
    v3.resize(7);
    /* 줄어든 3개의 객체는 메모리에 분명 남아 있습니다.
     * 하지만 소멸자를 호출해서 DB를 닫아야 합니다. 
     * 소멸자의 명시적 호출이 필요합니다.
     */
    v3.resize(8); 
    /* 늘어난 한개의 객체는 메모리에 이미 있습니다. 
     * 하지만 생성자를 호출해서 DB에 접속해야 합니다. 
     * 생성자의 명시적 호출이 필요합니다.
     */
    return 0;
}
```

## 5. new의 예외 상황
new 실패시 예외가 발생합니다. 할당에 실패 했을 경우 발생 합니다.  
try-catch를 이용해서 처리 할 수도 있지만 코드를 보기 싫어집니다.  
기존의 코드를 사용 못하는 경우도 생깁니다.   
1998 표준화 이전에 만든 코드가 있다면 아래 한줄만 추가한다.  
```#define new new(nothrow)```

```c++
#include <iostream>
#define nothrow 1
#define new new(nothrow)

using namespace std;

int main() {
    int *p2 = new(nothrow) int[100]; // 실패시 0리턴

    int *p = 0;
    try {
        p = new int[100];   // 실패시 예외 던짐(throw)
        // ...
        delete[] p;
    } catch (std::bad_alloc &e) {
        cout << "메모리 부족" << endl;
    }
}
```

## 6. nothrow 만들기
단지, 사용되지 않고 함수 오버로딩을 위한 인자가 필요 할때는 새로운 타입을 설계하는 것이 좋다 - "empty class" 개념  
```struct nothrow_t { }; // empty class - 아무 멤버도 없다.```  
```nothrow_t nothrow;  // sizeof(nothrow) => 1byte 입니다.```

```c++
#include <iostream>
using namespace std;

void *operator new(size_t sz) {
    void *p = malloc(sz);

    if (p == 0)
        throw std::bad_alloc(); // 예외 전달

    return p;
}

/* 아래 코드는 실제로 구현할 필요가 없습니다.
 * 이미 C++에서는 다음과 같은 방법으로 제공을 하고 있습니다.
 */
struct nothrow_t { };
nothrow_t nothrow;
void *operator new(size_t sz, nothrow_t n) {
    void *p = malloc(sz);
    return p;
}

int main() {
    int *p = new(nothrow) int[100];
    return 0;
}
```
